package NewsService.NewsService.controllers;

import NewsService.NewsService.models.News;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/news")
public class NewsController {

    @Autowired
    RestTemplate restTemplate;

    @GetMapping
    public List<News> list() {

          News[] news = restTemplate.getForObject("https://coronavirusapifr.herokuapp.com/data/live/france", News[].class);

        return Arrays.stream(news).collect(Collectors.toList());

    }

}
