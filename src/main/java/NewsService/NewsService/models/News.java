package NewsService.NewsService.models;

public class News {

        //Cumul des décès
        private int dc_tot;
        //nbr de nouveaux cas confirmés
        private int conf_j1;
        //nbr de personnes déclarées positives
        private int pos;
        //nbr de personnes en réanimation au cours des nouvelles 24h
        private int incid_rea;
        //nbr de nouveaux patients hospitalisés
        private int incid_hosp;

        public int getDc_tot() {
                return dc_tot;
        }

        public void setDc_tot(int dc_tot) {
                this.dc_tot = dc_tot;
        }

        public int getConf_j1() {
                return conf_j1;
        }

        public void setConf_j1(int conf_j1) {
                this.conf_j1 = conf_j1;
        }

        public int getPos() {
                return pos;
        }

        public void setPos(int pos) {
                this.pos = pos;
        }

        public int getIncid_rea() {
                return incid_rea;
        }

        public void setIncid_rea(int incid_rea) {
                this.incid_rea = incid_rea;
        }

        public int getIncid_hosp() {
                return incid_hosp;
        }

        public void setIncid_hosp(int incid_hosp) {
                this.incid_hosp = incid_hosp;
        }
}
